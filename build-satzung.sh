#!/bin/bash -xe

git clone https://$user:$deploytoken_documents@gitlab.com/upbhack/documents.git --depth 1 || true
latexmk -output-directory=/tmp/aux/ -interaction=nonstopmode -pdf documents/verein/satzung.tex
mv /tmp/aux/satzung.pdf static/
